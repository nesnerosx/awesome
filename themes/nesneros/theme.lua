---------------------------
-- nesneros awesome theme --
---------------------------

theme = {}

theme.font          = "sans 8"

theme.bg_normal     = "#30517a"
theme.bg_focus      = "#30517a"
theme.bg_urgent     = "#30517a"
theme.bg_minimize   = "#30517a"

theme.fg_normal     = "#85a6cf"
theme.fg_focus      = "#71d28f"
theme.fg_urgent     = "#85a6cf"
theme.fg_minimize   = "#85a6cf"

theme.border_width  = "1"
theme.border_normal = "#1b1b1b"
theme.border_focus  = "#71d28f"
theme.border_marked = "#71d28f"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Display the taglist squares
theme.taglist_squares_sel   = "~/.config/awesome/themes/nesneros/taglist/squarefw.png"
theme.taglist_squares_unsel = "~/.config/awesome/themes/nesneros/taglist/squarew.png"

theme.tasklist_floating_icon = "~/.config/awesome/themes/nesneros/tasklist/floatingw.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = "~/.config/awesome/themes/nesneros/submenu.png"
theme.menu_height = "15"
theme.menu_width  = "100"

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = "~/.config/awesome/themes/nesneros/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = "~/.config/awesome/themes/nesneros/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = "~/.config/awesome/themes/nesneros/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "~/.config/awesome/themes/nesneros/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "~/.config/awesome/themes/nesneros/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "~/.config/awesome/themes/nesneros/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "~/.config/awesome/themes/nesneros/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "~/.config/awesome/themes/nesneros/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "~/.config/awesome/themes/nesneros/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "~/.config/awesome/themes/nesneros/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "~/.config/awesome/themes/nesneros/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "~/.config/awesome/themes/nesneros/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "~/.config/awesome/themes/nesneros/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "~/.config/awesome/themes/nesneros/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "~/.config/awesome/themes/nesneros/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = "~/.config/awesome/themes/nesneros/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = "~/.config/awesome/themes/nesneros/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = "~/.config/awesome/themes/nesneros/titlebar/maximized_focus_active.png"

-- You can use your own command to set your wallpaper
theme.wallpaper_cmd = { "awsetbg -f .config/awesome/themes/nesneros/wallpaper_02.jpg" }

-- You can use your own layout icons like this:
theme.layout_fairh = "~/.config/awesome/themes/nesneros/layouts/fairhw.png"
theme.layout_fairv = "~/.config/awesome/themes/nesneros/layouts/fairvw.png"
theme.layout_floating  = "~/.config/awesome/themes/nesneros/layouts/floatingw_cust.png"
theme.layout_magnifier = "~/.config/awesome/themes/nesneros/layouts/magnifierw.png"
theme.layout_max = "~/.config/awesome/themes/nesneros/layouts/maxw.png"
theme.layout_fullscreen = "~/.config/awesome/themes/nesneros/layouts/fullscreenw.png"
theme.layout_tilebottom = "~/.config/awesome/themes/nesneros/layouts/tilebottomw.png"
theme.layout_tileleft   = "~/.config/awesome/themes/nesneros/layouts/tileleftw.png"
theme.layout_tile = "~/.config/awesome/themes/nesneros/layouts/tilew.png"
theme.layout_tiletop = "~/.config/awesome/themes/nesneros/layouts/tiletopw_cust.png"
theme.layout_spiral  = "~/.config/awesome/themes/nesneros/layouts/spiralw.png"
theme.layout_dwindle = "~/.config/awesome/themes/nesneros/layouts/dwindlew.png"

theme.awesome_icon = "~/.config/awesome/themes/nesneros/icons/nesneros16.png"

-- misc
theme.net_up_icon = "~/.config/awesome/themes/nesneros/icons/net_up.png"
theme.net_down_icon = "~/.config/awesome/themes/nesneros/icons/net_down.png"
theme.vol_icon = "~/.config/awesome/themes/nesneros/icons/vol.png"
theme.cpu_icon = "~/.config/awesome/themes/nesneros/icons/cpu.png"
theme.mem_icon = "~/.config/awesome/themes/nesneros/icons/mem.png"
theme.disk_icon = "~/.config/awesome/themes/nesneros/icons/disk.png"
theme.wifi_icon = "~/.config/awesome/themes/nesneros/icons/wifi.png"

-- mpd controls
theme.next_icon = "~/.config/awesome/themes/nesneros/mpd_icons/next.png"
theme.pause_icon = "~/.config/awesome/themes/nesneros/mpd_icons/pause.png"
theme.play_icon = "~/.config/awesome/themes/nesneros/mpd_icons/play.png"
theme.prev_icon = "~/.config/awesome/themes/nesneros/mpd_icons/previous.png"
theme.stop_icon = "~/.config/awesome/themes/nesneros/mpd_icons/stop.png"

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:encoding=utf-8:textwidth=80
